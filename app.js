var config = {
  apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
  authDomain: "get-send.firebaseapp.com",
  databaseURL: "https://get-send.firebaseio.com",
  storageBucket: "get-send.appspot.com"
};
// var khoangoai = { //nếu gia tri moi them vao có location trong table locations thì mới thêm
//   ".validate": "root.child('locations').child(newData.child('location').val()).exists()"
// }
firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    $("#state").html("đã đăng nhập " + user.email);
    // user.uid
  } else {
    $("#state").html("Chưa đăng nhập");
  }
});
var rootRef = firebase.database().ref();


$("document").ready(function () {
  $("#write").click(function (e) {
    let user = firebase.auth().currentUser;
    if (!user) {
      $("#state").val("chưa đn");
      return;
    }
    let content = $("#state").val();
    let uid = user.uid;
    // console.log(content);
    let dataUser = {
      email: user.email,
      username: content,
    }
    // get key
    let usersRef = rootRef.child('users');
    let daveRef = usersRef.child(uid);
    daveRef.set(dataUser).then(function (snapshot) {
      //console.log(snapshot);
    }, function (error) {
      $("#state").html("bạn chưa đc phép nhập vào đây đâu");
    });
  });
  $("#btnLogin").click(function (e) {
    var user = firebase.auth().currentUser;
    if (user) {
      // User is signed in.
    } else {
      // No user is signed in.
      var provider = new firebase.auth.GoogleAuthProvider();
      provider.addScope('profile');
      provider.addScope('email');
      firebase.auth().useDeviceLanguage();
      firebase.auth().signInWithPopup(provider).then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        console.log(user);
        // ...
      }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
      });
    }

  });
  $("#btnLoginEmail").click(function (e) {
    let email = $("#email").val();
    let pass = $("#pass").val();
    firebase.auth().signInWithEmailAndPassword(email, pass).then(function (value) {
      console.log(value);
    }, function (error) {
      let errorCode = error.code;
      let errorMessage = error.message;
      $("#state").html(errorMessage);
    });
  });
  $("#btnRegisEmail").click(function (e) {
    let email = $("#email").val();
    let pass = $("#pass").val();
    firebase.auth().createUserWithEmailAndPassword(email, pass).catch(function (error) {
      let errorCode = error.code;
      let errorMessage = error.message;
      messageError(errorMessage);
    });
  });
  $("#btnLogout").click(function (e) {
    firebase.auth().signOut().then(function () {
      $("#state").html("đăng xuất thành công");

    }).catch(function (error) {
      // An error happened.
    });
  });

});

function messageError(message) {
  $("#state").html(message);
}