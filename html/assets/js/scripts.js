(function ($) {

    $(document).ready(function () {
        mobileMenu();
        dataTables();
        dataTablesStatus();
        steptoggle();
        toggleHist();
    });

    function mobileMenu() {
        $('.navbar-toggle').mobileMenu({
            targetWrapper: '.ion-menu .block-menu',
        });
    };


    function dataTables() {
        if ($('#bookcar').length != 0) {
            var table = $('#bookcar').DataTable();
            $('#bookcar tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            $('#bookcar').dataTable({
                responsive: true,
                autoWidth: false,
            });

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        }
    };

    function steptoggle() {
        $('#action-step').on('click', function () {
            $('.block-step-sp').toggleClass('full-step');
            $('#action-map').show();
            $('#action-step').hide();
        });

        $('#action-map').on('click', function () {
            $('.block-step-sp').removeClass('full-step');
            $('#action-map').hide();
            $('#action-step').show();
        });

    };

    function toggleHist() {
        $('.btn-hist').on('click', function () {
            $('.page-home').toggleClass('page-history');
        });
        $('.histHide').on('click', function () {
            $('.page-home').removeClass('page-history');
        });
    };

    function dataTablesStatus() {
        if ($('#liststatus').length != 0) {
            var table1 = $('#liststatus').DataTable();
            $('#liststatus tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            $('#liststatus').dataTable({
                responsive: true,
                autoWidth: false,
                "order": [
                    [3, "desc"]
                ],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });

            // Apply the search
            table1.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        }
    }


})(jQuery);