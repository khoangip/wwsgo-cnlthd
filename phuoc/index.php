<?php  
  $conn = mysqli_connect('localhost', 'root', '', 'daunoi') or die ('Không thể kết nối tới database');

  mysqli_set_charset($conn,'utf8');
?>

<!DOCTYPE html><html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>BARG</title>
  <link rel="icon" href="favicon.png" sizes="16x16 32x32" type="image/png">
  <link href="./dist/css/style.min.css" rel="stylesheet">
</head>

<?php  
  $sql = 'SELECT * FROM city';

  $city = mysqli_query($conn, $sql);

  if(!$city)
  {
      die ('Câu truy vấn bị sai');
  }
?>

<body class="front">
  <div id="page" class="app-root">
    <span class="btn-close"></span>
    <div class="app-root position-relative clearfix">
      <div class="menu-enabled ion-menu">
        <div class="menu-inner">
          <div class="ion-content content content-md">
            <a class="text-center logo">
              <img src="logo.svg" width="67" height="47" alt="Back to Home"> WWSGO
            </a>
            <button id="main-menu" type="button" class="navbar-toggle collapsed">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="block-menu position-relative">
              <div class="scroll-content">
                <ul class="ion-list list list-md list-group">
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="menu-content ion-nav page-home">
        <div class="scroll-content">
          <div class="slide-content">
            <div class="slide-inner">
              <h1 class="title-page">
                Ghi nhận thông tin khách.
                <sup>
                  <span class="icon icon-md ion-ios-information-circle-outline"></span>
                </sup>
              </h1>
              <!-- <form action=""> -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="tel" id="sdt" placeholder="Số điện thoại" class="form-control" pattern="[0-9]{10,11}" required="required"  title="Số điện thoại">
                  </div>
                  <div class="form-group">
                    <select id="city" class="form-control" required="required" title="Tỉnh/ Thành phố">
                      <option value="">- Chọn Tỉnh/ Thành phố</option>
                      <?php 
                        while($row = mysqli_fetch_assoc($city))
                        {
                            echo '<option value="'.$row['tencity'].'">'.$row['tencity'].'</option>';
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <select id="quanhuyen" class="form-control" required="required" title="Quận/ huyện">
                      <option value="">- Chọn quận huyện -</option>
                    </select>
                  </div>
                  <div class="form-group">
                  	<select id="phuongxa" class="form-control" required="required" title="Phường/ xã">
                      <option value="">- Chọn phường xã -</option>
                    </select>
                  </div>
                  <div class="form-group">
                      <input type="text" class="form-control" id="district" placeholder="Nhập đường, thôn, xóm, làng.." required="required">
                    </div>
                  <div class="form-group">
                    <select id="loaixe" class="form-control" required="required" title="Loại xe">
                      <option value="">- Loại xe -</option>
                      <option value="1">Thường</option>
                      <option value="2">PREMIUM</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <textarea id="note" placeholder="Ghi chú thêm...!" class="form-control" rows="3" required="required" title="Ghi chú thêm...!"></textarea>
                  </div>
                  <button type="submit" id="xuly" class="btn btn-primary btn-block">Thêm</button>
                </div>
              </div>
          	  <!-- </form> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php  
    mysqli_free_result($city);
    mysqli_close($conn);
  ?>

  <script src="assets/includes/js/jquery.min.js"></script>
  <script src="assets/includes/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/mobile_menu.js"></script>
  <script src="assets/js/scripts.js"></script>

  <script type="text/javascript">
    $(document).ready(function(){
      $('#city').change(function(){ $id_city=$('#city').val();
        $.ajax({
          url: "./ajax_city.php",
          type: 'POST',
          cache: false,
          data: 'id_city='+$id_city,
          success: function(string){$('#quanhuyen').html(string)},
        });
        return false;
      });

      $('#quanhuyen').change(function(){ $id_qh=$('#quanhuyen').val();
        $.ajax({
          url: "./ajax_qh.php",
          type: 'POST',
          cache: false,
          data: 'id_qh='+$id_qh,
          success: function(string){$('#phuongxa').html(string)},
        });
        return false;
      });

    });
  </script>

  <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>

	<script>
  	var config = {
    	apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
    	authDomain: "get-send.firebaseapp.com",
    	databaseURL: "https://get-send.firebaseio.com",
    	projectId: "get-send.appspot.com",
  	};
  	firebase.initializeApp(config);

		var database = firebase.database();
    database.ref().child('khachhang').on('value',function(snap){
      console.log(snap.val());
    });
    database.ref().child('diem').on('value',function(snap){
      console.log(snap.val());
    });
    $(document).ready(function(){
      $('#sdt').blur(function(){
        
        var sdt=$("#sdt").val();

        ref = database.ref().child('diem');

        check_phone = firebase.database().ref().child('khachhang').orderByChild('sdt').equalTo(sdt);

        check_phone.once('value',function(snapshot)
        {
          var check_phone = snapshot.val();

          if(check_phone!=null)
          {
            ref.once('value', function(snapshot) {
              snapshot.forEach(function(childSnapshot) {
                var childKey = childSnapshot.key;
                var childData = childSnapshot.val();
                
                console.log(childData);
                
              });
            });
          }
        });  
      });
    });

		function writeNewPost(sdt,id_user,loaixe,phuongxa,quanhuyen,city,district,note,id_location,id_khachhang,id_taixe,status,created_time) 
		{
			ref = database.ref().child('diem');

			address = district + ', ' + phuongxa + ', ' + quanhuyen + ', ' + city;

			value_sdt = firebase.database().ref().child('khachhang').orderByChild('sdt').equalTo(sdt);

			value_sdt.once('value',function(snapshot)
			{
			    var test_sdt = snapshot.val();

			    if(test_sdt==null)
			    {
			    	var newPostKey = firebase.database().ref().child('khachhang').push().key;

					  var updates = {};

				  	var postKhachHang = { sdt: sdt, id_user: id_user };

				  	updates['/khachhang/' + newPostKey] = postKhachHang;

				  	var postDiem = 
				  	{
				  		loaixe : loaixe,
				    	address: address,
				    	note: note,
				    	id_location: id_location,
				    	id_khachhang: newPostKey,
				    	id_taixe: id_taixe,
				    	status: status,
				    	created_time: created_time
					 };

				  	updates['/diem/' + newPostKey] = postDiem;

				  	return firebase.database().ref().update(updates);
			    }
			    else
			    {
			    	ref.once('value', function(snapshot) {
  					  snapshot.forEach(function(childSnapshot) {
  					    var childKey = childSnapshot.key;
  					    var childData = childSnapshot.val();

  					    if(childData['address']==address)
  					    {
                  status=3;
                  console.log('in');
  					    }
                
  				    	var newPostKey = firebase.database().ref().child('diem').push().key;

  				    	var updates = {};

  						  var postDiem = 
  					  	{
  					  		loaixe : loaixe,
  					    	address: address,
  					    	note: note,
  					    	id_location: childData['id_location'],
  					    	id_khachhang: childData['id_khachhang'],
  					    	id_taixe: id_taixe,
  					    	status: status,
  					    	created_time: created_time
  						  };

  					  	updates['/diem/' + newPostKey] = postDiem;

  					  	firebase.database().ref().update(updates).catch(function(error){
                  console.log( error.message )
                }) ;

  					  });
  					});
			    }
			});
		}
	</script>

	<script type="text/javascript">

      function wordUpCase(str){
          return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
      }


     $(document).ready(function(){
       $('#xuly').click(function(e){

       	var loaixe=$("#loaixe").val();
       	var sdt=$("#sdt").val();
       	var id_user='';

       	var phuongxa=$("#phuongxa").val();
       	var quanhuyen=$("#quanhuyen").val();
       	var city=$("#city").val();
       	var district=wordUpCase($("#district").val());
       	var note=$("#note").val();

       	var id_location='';
       	var id_khachhang='';
       	var id_taixe='';

       	var status=2;

  			var created_time = Math.floor(Date.now() / 1000);

  			writeNewPost(sdt,id_user,loaixe,phuongxa,quanhuyen,city,district,note,id_location,id_khachhang,id_taixe,status,created_time);

       });
     });
	</script>

</body></html>