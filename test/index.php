<?php  
  $conn = mysqli_connect('localhost', 'root', '', 'daunoi') or die ('Không thể kết nối tới database');

  mysqli_set_charset($conn,'utf8');
?>

<!DOCTYPE html><html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>BARG</title>
  <link rel="icon" href="favicon.png" sizes="16x16 32x32" type="image/png">
  <link href="./dist/css/style.min.css" rel="stylesheet">
</head>

<?php  
  $sql = 'SELECT * FROM city';

  $city = mysqli_query($conn, $sql);

  if(!$city)
  {
      die ('Câu truy vấn bị sai');
  }
?>

<body class="front">
  <div id="page" class="app-root">
    <span class="btn-close"></span>
    <div class="app-root position-relative clearfix">
      <div class="menu-enabled ion-menu">
        <div class="menu-inner">
          <div class="ion-content content content-md">
            <a class="text-center logo">
              <img src="logo.svg" width="67" height="47" alt="Back to Home"> WWSGO
            </a>
            <button id="main-menu" type="button" class="navbar-toggle collapsed">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="block-menu position-relative">
              <div class="scroll-content">
                <ul class="ion-list list list-md list-group">
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                  <li>
                    <a href="#" class="list-group-item item">
                      <span class="fa fa-home" aria-hidden="true"></span>
                      Home
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="menu-content ion-nav page-home">
        <div class="scroll-content">
          <div class="slide-content">
            <div class="slide-inner">
              <h1 class="title-page">
                Ghi nhận thông tin khách.
                <sup>
                  <span class="icon icon-md ion-ios-information-circle-outline"></span>
                </sup>
              </h1>
              
              <div class="block-add-client">
                <div class="row">
                  <div class="col-lg-4 col-md-6 col-sm-8">
                    <form>
                      <div class="form-group">
                        <!-- <input type="tel" class="form-control" id="" placeholder="Số điện thoại" required="required"> -->
                        <input type="tel" class="form-control" id="sdt" placeholder="Số điện thoại" pattern="[0-9]{10,11}" required="required"  title="Số điện thoại">
                        <p class="help-block">
                          <a class="btn-hist histShow">
                            <i class="fa fa-history" aria-hidden="true"></i>
                            Xem lịch sử cuộc gọi
                          </a>
                        </p>
                      </div>
                      <div class="form-group">
                        <select id="city" class="form-control" required="required" title="Tỉnh/ Thành phố">
                          <option value="">- Chọn Tỉnh/ Thành phố</option>
                          <?php 
                            while($row = mysqli_fetch_assoc($city))
                            {
                                echo '<option value="'.$row['tencity'].'">'.$row['tencity'].'</option>';
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <select id="quanhuyen" class="form-control" required="required" title="Quận/ huyện">
                          <option value="">- Chọn quận huyện -</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <select id="phuongxa" class="form-control" required="required" title="Phường/ xã">
                          <option value="">- Chọn phường xã -</option>
                        </select>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control" id="district" placeholder="Nhập đường, thôn, xóm, làng.." required="required">
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control" id="sonhahem" placeholder="Nhập số nhà" required="required">
                      </div>
                      <div class="form-group">
                        <select id="loaixe" class="form-control" required="required" title="Loại xe">
                          <option value="">- Loại xe -</option>
                          <option value="1">Thường</option>
                          <option value="2">PREMIUM</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <textarea id="note" placeholder="Ghi chú thêm...!" class="form-control" rows="3" required="required" title="Ghi chú thêm...!"></textarea>
                      </div>
                      <button type="button" id="xuly" class="btn btn-primary btn-block">Thêm</button>
                    </form>
                  </div>
                </div>
              </div>
              <div class="block-list-history">
                <div class="row">
                  <div class="col-md-10">
                    <h1 class="title-page" id ="sdt-view" >
                      view sdt o day
                    </h1>
                    <table id="liststatus" class="table hover display" cellspacing="0" width="100%">
                    </table>
                  </div>
                  <div class="col-md-10">
                    <div class="action-history">
                      <a class="btn btn-primary histHide">Trở về</a>
                    </div>
                  </div>
                </div>
              </div>
          	  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php  
    mysqli_free_result($city);
    mysqli_close($conn);
  ?>

  <script src="assets/includes/js/jquery.min.js"></script>
  <script src="assets/includes/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/mobile_menu.js"></script>
  <script src="assets/includes/js/datatables.min.js"></script>
  <script src="lodash.js"></script>
  <script src="assets/js/scripts.js"></script>

  <script type="text/javascript">
    var table = $('#liststatus').DataTable({
      columns: [{
          title: "Địa chỉ",
        }, // 0
        {
          title: "Trạng thái",
        }, //1
        {
          title: "loại xe",
        }, // 2
        {
          title: "Thời gian",
        }, // 3
        {
          title: "Ghi chú",
        }, // 4 
      ]
    });
    $(document).ready(function(){
      $('.btn-hist').on('click', function () {
        $('.page-home').toggleClass('page-history');
      });
      $('.histHide').on('click', function () {
        $('.page-home').removeClass('page-history');
      });
      $('#city').change(function(){ $id_city=$('#city').val();
        $.ajax({
          url: "./ajax_city.php",
          type: 'POST',
          cache: false,
          data: 'id_city='+$id_city,
          success: function(string){$('#quanhuyen').html(string)},
        });
        return false;
      });

      $('#quanhuyen').change(function(){ $id_qh=$('#quanhuyen').val();
        $.ajax({
          url: "./ajax_qh.php",
          type: 'POST',
          cache: false,
          data: 'id_qh='+$id_qh,
          success: function(string){$('#phuongxa').html(string)},
        });
        return false;
      });

    });
  </script>

  <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>

	<script>
  	var config = {
    	apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
    	authDomain: "get-send.firebaseapp.com",
    	databaseURL: "https://get-send.firebaseio.com",
    	projectId: "get-send.appspot.com",
  	};
  	firebase.initializeApp(config);

		var database = firebase.database();
    database.ref().child('khachhang').once('value',function(snap){
      console.log(snap.val());
    });
    database.ref().child('diem').once('value',function(snap){
      console.log(snap.val());
    });
    $(document).ready(function(){
      $('#sdt').blur(function(){
        
        var sdt=$("#sdt").val();
        var rootRef = firebase.database().ref();
        rootRef.child('khachhang').orderByChild("sdt").equalTo(sdt).once('value',function(snap){
          if(snap.val()){
            $("#sdt-view").html(sdt);
            // console.log(snap.val());
            rootRef.child('diem').orderByChild('id_khachhang').equalTo( Object.keys( snap.val() )[0]).once('value',function(snap){
              // console.log(snap.val());
              if( snap.val() ){
                table.clear().draw();
                snap.forEach(function(childSnap){
                  let typeStatus = '';
                  let snapKey = childSnap.key;
                  let snapVal = childSnap.val();
                  let loaixe = '';
                  if( snapVal.loaixe == 1 ){
                    loaixe = '<img src="./assets/images/vespa.svg" width="40" height="40" alt="">';
                  }else{
                    loaixe = '<img src="./assets/images/bike.svg" width="40" height="40" alt="">';
                  }
                    
                  if (typeof snapVal.created_time === 'undefined') {
                    snapVal.created_time = "0";
                  }
                  if( snapVal.status == 1 ){
                    typeStatus = '<strong class="warning"> Đã định vị xong</strong>';
                  }else if( snapVal.status == 2 ){
                    typeStatus = '<strong class="danger">Chưa được định vị</strong>' ;
                  }else if( snapVal.status == 3 ){
                    typeStatus = "Đang định vị";
                  }else if( snapVal.status == 4 ){
                    typeStatus = '<strong class="info">Đang di chuyển </strong>'
                  }else if( snapVal.status == 5 ){
                    typeStatus = '<strong class="success">Hoàn tất đường đi</strong>';
                  }else if( snapVal.status == 6 ){
                    typeStatus = '<strong class="success">Đã có xe nhận</strong>';
                  }else if( snapVal.status == 7 ){
                    
                  }else if( snapVal.status == 8 ){
                    typeStatus = "Hệ thống đang chọn xe";
                  }
                  table.row.add([snapVal.sonhahem + " " + snapVal.address, typeStatus , loaixe , convertTimestamToDate(snapVal.created_time) , snapVal.note
                  ]).draw(false);
                });
              }
            }) ;
             // endat deprecated use limitToFirst 
          }
        });
      });
    });

		function writeNewPost(sdt,id_user,loaixe,phuongxa,quanhuyen,city,district,sonhahem,note,id_location,id_khachhang,id_taixe,status,created_time) 
		{
			ref = database.ref().child('diem');

			address = district + ', ' + phuongxa + ', ' + quanhuyen + ', ' + city;

      value_sdt = firebase.database().ref().child('khachhang').orderByChild('sdt').equalTo(sdt);
      var idKH = '';
			value_sdt.once('value',function(snapshot)
			{
          var test_sdt = snapshot.val();
          console.log(snapshot.val() );
			    if(test_sdt==null)
			    {
			    	var newPostKey = firebase.database().ref().child('khachhang').push().key;
					  var updates = {};

				  	var postKhachHang = { sdt: sdt, id_user: id_user };

				  	updates['/khachhang/' + newPostKey] = postKhachHang;

				  	var postDiem = 
				  	{
				  		loaixe : loaixe,
				    	address: address,
				    	note: note,
				    	id_location: id_location,
				    	id_khachhang: newPostKey,
              id_taixe: id_taixe,
              sonhahem: sonhahem,
				    	status: status,
				    	created_time: created_time
					 };
           var newPostKeyDiem = firebase.database().ref().child('diem').push().key;
				  	updates['/diem/' + newPostKeyDiem] = postDiem;
				  	return firebase.database().ref().update(updates);
			    }
			    else
			    {
            // "a"
            i = 0; childAdd = {};
			    	ref.once('value', function(snapshot) {
  					  snapshot.forEach(function(childSnapshot) {
  					    var childKey = childSnapshot.key;
  					    var childData = childSnapshot.val();
  					    if(childData.address ==address)
  					    {
                  status=1;
                  childAdd = childData;
                }
                i++;
                if (i == _.filter(snapshot.val(), function (o) {
                    if (!_.isEmpty(o)) return o;
                }).length - 1) { // fuck promise
                  // do some thiong
                  if( typeof childAdd.loaixe === 'undefined' ){
                    childAdd = childData;
                    childAdd.location = '0';
                  }
                  var newPostKey = firebase.database().ref().child('diem').push().key;
                  var updates = {};
                  if( typeof childAdd.location === 'undefined' ){
                    childAdd.location = '0';
                  }
                  var postDiem = 
                  {
                    loaixe : loaixe,
                    address: address,
                    note: note,
                    sonhahem: sonhahem,
                    location: childAdd.location,
                    id_khachhang: Object.keys(test_sdt)[0]  ,
                    id_taixe: id_taixe,
                    status: status,
                    created_time: created_time
                  };
                  console.log(postDiem);
                  // updates['/diem/' + newPostKey] = postDiem;

                  return firebase.database().ref('diem/'+newPostKey).set(postDiem);
                }
  					  });
  					});
			    }
			});
		}
	</script>

	<script type="text/javascript">
function convertTimestamToDate(timestamp) {
  var date = new Date(timestamp*1000);
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();

  // Will display time in 10:30:23 format
  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  return day + '/' + month + '/' + year + ' ' + formattedTime;
}
      function wordUpCase(str){
          return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
      }


     $(document).ready(function(){
       $('#xuly').click(function(e){

       	var loaixe=$("#loaixe").val();
       	var sdt=$("#sdt").val();
       	var id_user='';

       	var phuongxa=$("#phuongxa").val();
       	var quanhuyen=$("#quanhuyen").val();
       	var city=$("#city").val();
        var district=wordUpCase($("#district").val());
        var sonhahem = $("#sonhahem").val();
       	var note=$("#note").val();

       	var id_location='';
       	var id_khachhang='';
       	var id_taixe='';

       	var status=2;

  			var created_time = Math.floor(Date.now() / 1000);

  			writeNewPost(sdt,id_user,loaixe,phuongxa,quanhuyen,city,district,sonhahem,note,id_location,id_khachhang,id_taixe,status,created_time);

       });
     });
	</script>

</body></html>