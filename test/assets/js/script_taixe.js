$(document).ready(function (e) {
  $(".taixe-run").hide();
  // $("#page").removeClass('modal-open');
  $("#taixe-accept").click(function (e) {
    $("#page").removeClass('modal-open');
  });
  $("#taixe-accept-deny").click(function (e) {
    $("#page").removeClass('modal-open');
    console.log('từ chối');
  });

  $("#taixe-accept-argee").click(function (e) {
    clearTimeout(myTimeout);
    clearInterval(myInterval);
    $("#page").removeClass('modal-open');
    $(".taixe-run").show();
    //$("#taixe-run").toggle();
    console.log('Đồng ý');
    if (!_.isEmpty(directionsDisplay)) {
      hideDirection();
    }
    taixeRuning();
  });

  $("#taixe-run-start").click(function (e) {
    // click bắt đầu , update status tai xe , update status dangkyxe
    $(this).hide(0, function () {
      $("#taixe-run-end").show();
      $("#header-panel").html("Đoạn đường của khách <span>(km)</span>");
      $("#header-right").html("");
      hideDirection();
    });
    rootRef.child('tai').child(taixe.key).update({
      status: 2
    });
    rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).update({
      status: 2
    });
  });
  $("#taixe-run-end").click(function (e) {
    // click ket thuc , update tai xe ve dang san sang, update vi tri da xong
    $(this).hide(0, function () {
      $("#taixe-run-start").show();
      $(".taixe-run").hide();
    });
    rootRef.child('tai').child(taixe.key).update({
      status: 1
    });
    rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).update({
      status: 1
    });
    rootRef.child('diem').child(taixe.data.waitAccept.id_diem).update({
      status: 5
    })
  });
});
var m1, bd1, markMove, markPrev, myTimeout, myInterval;
var taixe = {};
var directionsDisplay;
var directionsService;
var config = {
  apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
  authDomain: "get-send.firebaseapp.com",
  databaseURL: "https://get-send.firebaseio.com",
  storageBucket: "get-send.appspot.com"
};
firebase.initializeApp(config);
var rootRef = firebase.database().ref();
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    console.log("đã đăng nhập " + user.email);
    // user.uid
    //console.log(user.uid);
    rootRef.child('tai').orderByChild('uid').equalTo(user.uid /* optional */ ).once('value', function (snap) {
      snap.forEach(function (childSnap) {
        taixe.data = childSnap.val();
        taixe.key = childSnap.key;
        hienbando(taixe.data.location, ['loadmap', 'loadmarker', 'move']);
        // getLatLng(taixe.data.id_location).then(function(toado){
        //   hienbando(toado.lat,toado.lng,['loadmap','loadmarker','move']);
        // });
      });
    });
    rootRef.child('tai').orderByChild('uid').equalTo(user.uid /* optional */ ).on('child_changed', function (snap) {
      taixe.data = snap.val();
      taixe.key = snap.key;
      if (!_.isEmpty(snap.val().waitAccept) && (snap.val().waitAccept.new === 1)) {
        $(".sdt").html(snap.val().waitAccept.sdt);
        $(".description").html("khách hàng " + snap.val().waitAccept.address + " <br> cách <b>" + (snap.val().waitAccept.distance / 1000) + " km</b>");
        $("#page").addClass('modal-open');

        $("#taixe-accept-deny").removeAttr("disabled");
        $("#taixe-accept-argee").removeAttr("disabled");
        var i = 5;
        $('.count-down').html(i);
        myInterval = setInterval(function () {
          --i;
          $('.count-down').html(i);
        }, 1000);
        myTimeout = setTimeout(() => {
          let updates = {};
          clearInterval(myInterval);
          i = 0;
          $("#taixe-accept-deny").attr("disabled", "disabled");
          $("#taixe-accept-argee").attr("disabled", "disabled");
          $(".sdt").html('HẾT HẠN');
          $(".description").html('Quá hạn xác nhận');
          updates['/waitAccept'] = {
            new: 0
          };
          updates['/status'] = 1;
          rootRef.child('tai').child(taixe.key).update(updates);
        }, 6000);

      }
      m1.setMap(null);
      hienbando(taixe.data.location, ['loadmarker']);

      if (snap.val().status == 3) {
        //directionsDisplay.setMap(null);
        taixeRunningLocation(taixe.data.location);
      }
      // getLatLng(taixe.data.id_location).then(function(toado){
      //   hienbando(toado.lat,toado.lng,['loadmarker']);
      // });
    });
    // endat deprecated use limitToFirst 
    guiToaDoLen(user);
  } else {
    console.log("chuaa đăng nhập ");
  }
});

/** function */
function hideDirection() {
  directionsDisplay.setMap(null);
  directionsDisplay.setDirections({
    routes: []
  });
}

function getKhoangCach(locationBatDau, locationKetThuc) {

  var service = new google.maps.DistanceMatrixService();

  service.getDistanceMatrix({
    origins: [locationBatDau],
    destinations: [locationKetThuc],
    travelMode: 'DRIVING',
    unitSystem: google.maps.UnitSystem.METRIC,
    avoidHighways: false,
    avoidTolls: false,
  }, function (response, status) {
    if (status !== 'OK') {
      alert('Error was getKhoangCach: ' + status);
    } else {
      var originList = response.originAddresses;
      var destinationList = response.destinationAddresses;
    }

    for (var i = 0; i < originList.length; i++) {
      let results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
        // outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
        //           ': ' + results[j].distance.text + ' in ' +
        //           results[j].duration.text + '<br>';
        // tmp.push({
        //   location: originArray.dataLocation[i],
        //   distance: results[j].distance.value,
        //   data: originArray.data[i]
        // });
        $("#header-panel").html(results[j].duration.text + "<span>(" + results[j].distance.text + ")</span>");
      }
    }
  });
}

function taixeRuning() {
  console.log(' in running');
  rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).once('value', function (snap) {
    console.log(snap.val());
    if (snap.val()) {
      if (snap.val().status == 3) {
        rootRef.child('diem').child(taixe.data.waitAccept.id_diem).update({
          status: 6
        });
        let updates = {};
        console.log(taixe.data);
        updates['/status'] = 3;
        updates['/waitAccept'] = {
          ChuaXN: 0,
          address: taixe.data.waitAccept.address,
          id_dangkyxe: taixe.data.waitAccept.id_dangkyxe,
          new: 0,
          sdt: taixe.data.waitAccept.sdt
        }
        rootRef.child('tai').child(taixe.key).update(updates);
        rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).update({
          id_taixe: taixe.data.id_taixe,
          status: 4 // tai xe xn
        }).catch(function (error) {
          console.log(error.message);
        });
      } else {
        alert(' đã có người pick khách này');
      }
      directionsDisplay = new google.maps.DirectionsRenderer;
      directionsService = new google.maps.DirectionsService;
      directionsDisplay.setMap(bd1);
      directionsDisplay.setPanel(document.getElementById('right-panel'));
      taixeRunningLocation();
    }
  });

  // rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).update({
  //   status: 2
  // });

}

function taixeRunningLocation(locationStart = {}) {
  rootRef.child('dangkyxe').child(taixe.data.waitAccept.id_dangkyxe).once('value', function (snap) {
    let snapVal = snap.val();
    if (!_.isEmpty(locationStart)) {
      snapVal.start = locationStart;
    }
    if (typeof snapVal.start === 'undefined') {
      snapVal.start = taixe.data.location;
    }

    // var control = document.getElementById('floating-panel'); dis plaay con trol to map
    //   control.style.display = 'block';
    //   map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);
    calculateAndDisplayRoute(directionsService, directionsDisplay, snapVal);

  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay, data) {
  console.log(data);

  // var start = document.getElementById('start').value;
  // var end = document.getElementById('end').value;
  directionsService.route({
    origin: data.start,
    destination: data.des,
    travelMode: 'DRIVING'
  }, function (response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
  getKhoangCach(data.start, data.des);
  // goi lay khoang cach bat dau ket thuc
}

function hienbando(location, action) {
  switch (taixe.data.status) {
    case 1:
      $('.title-page').html('Đang sẵn sàng.<sup><span class="icon icon-md ion-ios-information-circle-outline"></span></sup>');
      break;
    case 2:
      $('.title-page').html('Hãy kiểm soát tốc độ.<sup><span class="icon icon-md ion-ios-information-circle-outline"></span></sup>');
      break;
    case 3:
      $('.title-page').html('Khách hàng đang chờ bạn.<sup><span class="icon icon-md ion-ios-information-circle-outline"></span></sup>');
      break;
    default:
      $('.title-page').html('Chưa sẵn sàng.<sup><span class="icon icon-md ion-ios-information-circle-outline"></span></sup>');
  }
  var geoData;
  if (_.indexOf(action, 'loadmap') != -1) {
    var opt = {
      center: new google.maps.LatLng(location),
      zoom: 14,
      mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP/SATELLITE/HYBRID/TERRAIN
    };
    bd1 = new google.maps.Map(document.getElementById("map_canvas"), opt);
  }

  if (_.indexOf(action, 'loadmarker') != -1) {
    m1 = new google.maps.Marker({
      position: new google.maps.LatLng(location),
      map: bd1,
      title: 'tai xe'
    });

    let constant = '<div id="content">' +
      '<div id="siteNotice">' + '</div>' +
      '<h1 id="firstHeading" class="firstHeading"></h1>' +
      '<div id="bodyContent">' +
      '<p><b>Tài xế</b></p>' +
      '<p><b>Sdt</b>: ' + taixe.data.sdt + '</p>' +
      '<p><b>tên</b>: ' + taixe.data.ten + '</p>' +
      '<p><b>cmnd</b>: ' + taixe.data.cmnd + '</p>' +
      '</div>' + '</div>';
    let infowindow = new google.maps.InfoWindow({
      content: constant
    });
    m1.addListener('click', function (e) {
      infowindow.open(bd1, m1);
    });
  }
  //UpLatLng(lat,lng,"-1");// thêm toạ độ trung tâm
  if (_.indexOf(action, 'move') != -1) {
    dichuyen();
  }
}

function getLatLng(id_location) {
  return rootRef.child('location').child(id_location).once('value').then(function (snap) {
    return snap.val();
  }).catch(function (err) {
    return "";
  });
}

function dichuyen() {
  google.maps.event.addListener(bd1, "click", function (e) {
    markMove = {
      lat: e.latLng.lat(),
      lng: e.latLng.lng()
    };
  });
}

function guiToaDoLen(user) {
  var updates;
  if (!user) {
    console.log("chưa đn ");
    return;
  }
  setInterval(function () {
    if (!_.isEqual(markMove, markPrev) || (!markPrev && !_.isEmpty(markMove))) {

      markPrev = markMove;
      console.log(markMove);

      // get key
      // let locationRef = rootRef.child('location');
      // var newpostKey = locationRef.push().key;
      // let daveRef = locationRef.child(newpostKey);
      //taixe.data.id_location = newpostKey;

      rootRef.child('tai').child(taixe.key).child('location').update(markMove).then(function () {
        console.log('update tai xe location');
      });
    }

  }, 5000);


}