(function ($) {

    $(document).ready(function () {
        mobileMenu();
        //dataTables();
        //dataTablesStatus();
        steptoggle();
        $("#btnLogout").click(function (e) {
            firebase.auth().signOut().then(function () {
                //logout thành công
                alert('logout thành công');
            }).catch(function (error) {
                // An error happened.
            });
        });
    });

    function mobileMenu() {
        $('.navbar-toggle').mobileMenu({
            targetWrapper: '.ion-menu .block-menu',
        });
    }

    function dataTables() {
        if ($('#bookcar').length != 0) {
            $('#bookcar tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            $('#bookcar').dataTable({
                responsive: true,
                autoWidth: false,
            });

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        }
    }

    function steptoggle() {
        $('#action-step').on('click', function () {
            $('.block-step-sp').toggleClass('full-step');
            $('#action-map').show();
            $('#action-step').hide();
        });

        $('#action-map').on('click', function () {
            $('.block-step-sp').removeClass('full-step');
            $('#action-map').hide();
            $('#action-step').show();
        });

    };

    function dataTablesStatus() {
        if ($('#liststatus').length != 0) {
            $('#liststatus tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Search ' + title + '" />');
            });

            $('#liststatus').dataTable({
                responsive: true,
                autoWidth: false,
                "order": [
                    [3, "desc"]
                ],
                "columnDefs": [{
                    "targets": 'no-sort',
                    "orderable": false,
                }]
            });

            // Apply the search
            table.columns().every(function () {
                var that = this;

                $('input', this.footer()).on('keyup change', function () {
                    if (that.search() !== this.value) {
                        that
                            .search(this.value)
                            .draw();
                    }
                });
            });
        }
    }


})(jQuery);