var geocoder = new google.maps.Geocoder();
var config = {
  apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
  authDomain: "get-send.firebaseapp.com",
  databaseURL: "https://get-send.firebaseio.com",
  storageBucket: "get-send.appspot.com"
};
firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    console.log("đã đăng nhập " + user.email);
    // user.uid
  } else {
    alert("chưa đăng nhập");
  }
});
var table = $('#bookcar').DataTable({
  responsive: true,
  autoWidth: false,
  'fnCreatedRow': function (nRow, aData, iDataIndex) {
    console.log(aData[0]);
    $(nRow).attr('class', aData[0]); // or whatever you choose to set as the id
  },
  columns: [{
      title: "id",
      visible: false
    }, // 0
    {
      title: "location",
      visible: false
    }, //1
    {
      title: "id_taixe",
      visible: false
    }, // 2
    {
      title: "id_khachhang",
      visible: false
    }, // 3
    {
      title: "created_time",
      visible: false
    }, // 4 
    {
      title: "sonhahem",
      visible: false
    }, // 5
    {
      title: "address",
      visible: false
    }, // 6
    {
      title: "Số điện thoại"
    }, // 7 
    {
      title: "Loại Xe"
    }, // 8 
    {
      title: "thời gian đặt"
    }, // 9
    {
      title: "Địa chỉ"
    }, // 10
    {
      title: "Ghi chú"
    }, // 11 note
    {
      title: "trạng thái"
    } // 12 status
  ]
});
var rootRef = firebase.database().ref();
var diemRef = rootRef.child('diem');
var dangkyxeRef = rootRef.child('dangkyxe');
diemRef.on('child_changed', function (data) {
  var listDiem = [];
  // table.clear().draw();
  // snap.forEach(function (childSnap) {
  let snapKey = data.key;
  let snapVal = data.val();
  snapVal.id_diem = snapKey;
  if (typeof snapVal.status !== 'undefined' && snapVal.status == '1') {
    fetchKH(snapVal.id_khachhang).then(function (dataKH) {
      // lấy trang thai đã đc định vị
      var updatesDiem = {
        updated_time: Date.now(),
        status: 8 // he thong dang chọn xe
      };
      snapVal.sdt = dataKH.sdt;
      rootRef.child('/diem/' + snapKey).update(updatesDiem).then(function () {
        // update thah cong
        let loaixe = (snapVal.loaixe == 1) ? "xe thường" : "xe vip";

        if (typeof snapVal.created_time === 'undefined') {
          snapVal.created_time = "0";
        }
        if (_.isEmpty(snapVal.location)) {
          snapVal.location = 0;
        }
        var statusField = "hệ thống đang chọn xe";

        // 0 id_khachahng , 1 location , 2 id_taixe , 3 id_khachhang, 4 created_time , 5 sonhahem,  6 address,7 sdt, 8 loai xe, 9 address , 10 note,
        table.row.add([snapKey, snapVal.location, '123', snapVal.id_khachhang, snapVal.created_time, snapVal.sonhahem, snapVal.address, dataKH.sdt, loaixe, convertTimestamToDate(snapVal.created_time), snapVal.sonhahem + " " + snapVal.address, snapVal.note, statusField]).draw(false);
        console.log(snapVal);
        if (typeof snapVal.location === 'object') {
          getTaixe(snapVal.location, snapVal);
        } else {
          geoData = snapVal.sonhahem + " " + snapVal.address;
          getTaixe(geoData, snapVal);
        }
      }, function (err) {
        // err here
        console.log('update diem lỗi');
      });
    });
  }
  // });
});
diemRef.on('child_added', function (data) {
  var listDiem = [];
  // table.clear().draw();
  // snap.forEach(function (childSnap) {
  let snapKey = data.key;
  let snapVal = data.val();
  snapVal.id_diem = snapKey;
  if (typeof snapVal.status !== 'undefined' && snapVal.status == '1') {
    fetchKH(snapVal.id_khachhang).then(function (dataKH) {
      // lấy trang thai đã đc định vị
      var updatesDiem = {
        updated_time: Date.now(),
        status: 8 // he thong dang chọn xe
      };
      snapVal.sdt = dataKH.sdt;
      rootRef.child('/diem/' + snapKey).update(updatesDiem).then(function () {
        // update thah cong
        let loaixe = (snapVal.loaixe == 1) ? "xe thường" : "xe vip";

        if (typeof snapVal.created_time === 'undefined') {
          snapVal.created_time = "0";
        }
        if (_.isEmpty(snapVal.location)) {
          snapVal.location = 0;
        }
        var statusField = "hệ thống đang chọn xe";

        // 0 id_khachahng , 1 location , 2 id_taixe , 3 id_khachhang, 4 created_time , 5 sonhahem,  6 address,7 sdt, 8 loai xe, 9 address , 10 note,
        table.row.add([snapKey, snapVal.location, '123', snapVal.id_khachhang, snapVal.created_time, snapVal.sonhahem, snapVal.address, dataKH.sdt, loaixe, convertTimestamToDate(snapVal.created_time), snapVal.sonhahem + " " + snapVal.address, snapVal.note, statusField]).draw(false);
        console.log(snapVal);
        if (typeof snapVal.location === 'object') {
          getTaixe(snapVal.location, snapVal);
        } else {
          geoData = snapVal.sonhahem + " " + snapVal.address;
          getTaixe(geoData, snapVal);
        }
      }, function (err) {
        // err here
        console.log('update diem lỗi');
      });
    });
  }
  // });
});
$(document).ready(function () {
  // dataTables();
});

function getTaixe(addressKH, dataDiem) {
  var taixeRef;
  taixeRef = rootRef.child('tai').orderByChild('status').equalTo(1);
  // endat deprecated use limitToFirst ;
  taixeRef.once('value', function (snap) {
    var listTaixe = {};
    listTaixe.dataLocation = [];
    listTaixe.data = [];
    var i = 0;
    snap.forEach(function (childSnap) {

      let snapKey = childSnap.key;
      let snapVal = childSnap.val();
      if (snapVal.status == 1 && typeof snapVal.location !== 'undefined') {
        listTaixe.dataLocation.push(snapVal.location);
        snapVal.pathKey = snapKey;
        listTaixe.data.push(snapVal);
        i++;
      }

    });
    if (i == _.filter(snap.val(), function (o) {
        if (!_.isEmpty(o) && typeof o.location !== 'undefined') return o;
      }).length) { // fuck promise
      getDistance(listTaixe, [addressKH], dataDiem);
    }
    //getDistance(listTaixe,[addressKH] );
  });
}

function getDistance(originArray, desinationArray, dataDiem) {
  // var origin1 = {lat: 10.764036451389597, lng: 106.66014635961915};
  // var origin2 = { lat:10.755435640815035 ,lng: 106.68615305822755 };
  // var destinationA = 'Stockholm, Sweden';
  // var destinationB = {lat: 10.762603, lng: 106.682634};
  var service = new google.maps.DistanceMatrixService();
  var tmp = [];
  //originArray = [ origin1,origin2, { lat: 10.771456561337404, lng: 106.71868288916016 } , { lat: 10.74970163083448, lng: 106.71713793676759 }, { lat: 10.741606372460339, lng: 106.69799769323731 }, { lat: 10.740004160180058, lng: 106.71662295263673 }   ];
  //desinationArray = [destinationB];
  service.getDistanceMatrix({
    origins: originArray.dataLocation,
    destinations: desinationArray,
    travelMode: 'DRIVING',
    // transitOptions: TransitOptions,
    // drivingOptions: DrivingOptions,
    unitSystem: google.maps.UnitSystem.METRIC,
    avoidHighways: false,
    avoidTolls: false,
  }, function (response, status) {
    if (status !== 'OK') {
      alert('Error was: ' + status);
    } else {
      var originList = response.originAddresses;
      var destinationList = response.destinationAddresses;

      //deleteMarkers(markersArray);
      //markersArray = [];
      for (var i = 0; i < originList.length; i++) {
        let results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          // outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
          //           ': ' + results[j].distance.text + ' in ' +
          //           results[j].duration.text + '<br>';
          tmp.push({
            location: originArray.dataLocation[i],
            distance: results[j].distance.value,
            data: originArray.data[i]
          });
        }
        if (i == (originList.length - 1)) {
          tmp = _.take(_.orderBy(tmp, ['distance'], ['asc']), 10);
        }
      }
      var demI = 0; //  set your counter to 1

      var dungVongLap = 1
      // insert dang ky xe
      var newpostKey = dangkyxeRef.push().key;
      let insertDangkyxeRef = dangkyxeRef.child(newpostKey);

      let locationTmp = {};
      var geodata;
      console.log(dataDiem);
      if (typeof dataDiem.location != 'object') {
        console.log(' ta in ');
        geoData = "588 lê đức thọ, p15, gò vấp";
        geocoder.geocode({
          'address': geoData,
        }, function (results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            locationTmp = results[0].geometry.location;
          } else {
            alert("Geocode was not successful for the following reason: " + status);
          }
        });
      } else {
        locationTmp = dataDiem.location;
      }
      console.log(locationTmp);
      let postData = {
        created_time: Date.now(),
        des: locationTmp,
        id_diem: dataDiem.id_diem,
        id_taixe: '',
        status: 3
      };

      insertDangkyxeRef.set(postData).then(function () {
        console.log(' thêm dang ky xe thành công');
      }, function (error) {
        console.log('loi insert');
        console.log(error.message);
      });
      // end insert dang ky xe
      function myLoop() {
        setTimeout(function () {
          if (demI in tmp) { //if ("home" in assoc_pagine)

            if (tmp[demI].data.status != 1) {
              console.log(' tai xe ' + tmp[demI].data.ten + ' chưa sẳn sàng');
            } else {
              // console.log('đã dinh vi');
              // return;
              console.log(' tai xe ' + tmp[demI].data.ten + ' sẳn sàng');
              if (tmp[demI].data) {
                var updates = {};
                updates['/status'] = 4;
                updates['/waitAccept'] = {
                  ChuaXN: 1,
                  id_dangkyxe: newpostKey,
                  address: dataDiem.sonhahem + dataDiem.address,
                  sdt: dataDiem.sdt,
                  new: 1,
                  distance: tmp[demI].distance,
                  id_diem: dataDiem.id_diem
                };
                rootRef.child('tai').child(tmp[demI].data.pathKey).update(updates).then(function () {

                }).catch(function (err) {
                  console.log(err.message);
                  console.log('check update taixe status , waite accept');
                });
                console.log(' đang chờ xác nhận');

                // setTimeout(function () {
                //   console.log(' time out');
                //   console.log(demI);
                //   rootRef.child('dangkyxe').child(newpostKey).once('value', function (snap) {
                //     if (snap.val()) {
                //       console.log(snap.val());
                //       if (snap.val().status == 4) {
                //         rootRef.child('diem').child(dataDiem.id_diem).update({
                //           status: 6
                //         }).then(function () {
                //           table.row($("." + dataDiem.id_diem)).remove().draw();
                //         });
                //         dungVongLap = 0;
                //       }
                //     }
                //   });
                // }, 4000);

              }
            }
          }
          demI++;
          if (demI < 10 && dungVongLap == 1) {
            myLoop();
          } else {}
        }, 7000);
      }
      myLoop();

    }
  });

}

function convertTimestamToDate(timestamp) {
  var date = new Date(timestamp);
  // Hours part from the timestamp
  var hours = date.getHours();
  // Minutes part from the timestamp
  var minutes = "0" + date.getMinutes();
  // Seconds part from the timestamp
  var seconds = "0" + date.getSeconds();

  // Will display time in 10:30:23 format
  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var day = date.getDate();
  return day + '/' + month + '/' + year + ' ' + formattedTime;
}

function updateDiem(data, callback) {
  khachhang.data = data;
  khachhang.location = {};
  khachhang.mark = true;
  var postData = {
    updated_time: Date.now(),
    created_time: data[4],
    location: data[1],
    id_taixe: data[2],
    id_khachhang: data[3],
    address: data[6],
    note: data[10],
    status: 3, // dang xu ly
    sonhahem: data[5],
    uid: data[11]
  }
  // var updates = {};
  // updates['/diem/' + data[0]  ] = postData;
  rootRef.child('/diem/' + data[0]).set(postData).then(callback(postData), function (err) {
    // err here
  });
}

function fetchKH(id_khachhang) {
  return rootRef.child('khachhang').child(id_khachhang).once('value').then(function (snap) {
    return snap.val();
  });
}

function dataTables() {
  if ($('#bookcar').length != 0) {
    $('#bookcar tfoot th').each(function () {
      var title = $(this).text();
      $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    });

    $('#bookcar').dataTable({
      responsive: true,
      autoWidth: false,
    });

    // Apply the search
    table.columns().every(function () {
      var that = this;

      $('input', this.footer()).on('keyup change', function () {
        if (that.search() !== this.value) {
          that
            .search(this.value)
            .draw();
        }
      });
    });
  }
}