var config = {
  apiKey: "AIzaSyCfckC0S6A5HNK2TGiuqwajeDnbCcKf97c",
  authDomain: "get-send.firebaseapp.com",
  databaseURL: "https://get-send.firebaseio.com",
  storageBucket: "get-send.appspot.com"
};
firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    console.log("đã đăng nhập " + user.email);
    // user.uid
  } else {
    alert("chưa đăng nhập");
  }
});

var table = $('#bookcar').DataTable({
  responsive: true,
  autoWidth: false,
  'fnCreatedRow': function (nRow, aData, iDataIndex) {
    console.log(aData[0]);
    $(nRow).attr('class', aData[0]); // or whatever you choose to set as the id
  },
  columns: [{
      title: "id",
      visible: false
    }, // 0
    {
      title: "location",
      visible: false
    }, //1
    {
      title: "id_taixe",
      visible: false
    }, // 2
    {
      title: "id_khachhang",
      visible: false
    }, // 3
    {
      title: "created_time",
      visible: false
    }, // 4 
    {
      title: "sonhahem",
      visible: false
    }, // 5
    {
      title: "address",
      visible: false
    }, // 6
    {
      title: "Số điện thoại"
    }, // 7 
    {
      title: "Loại Xe"
    }, // 8 
    {
      title: "Địa chỉ"
    }, // 9 
    {
      title: "Tình trạng"
    }, // 10
    {
      title: "Ghi chú"
    }, // 11
    {
      title: "status",
      visible: false
    } // 12
  ]
});

var rootRef = firebase.database().ref();
var locationRef = rootRef.child('location');
var khachhang = {};
var bd1;
var markersArray = [];
var m1; // marker khachhang
var diemRef = rootRef.child('diem');
diemRef.on('value', function (snap) {
  var listDiem = [];
  table.clear().draw();
  snap.forEach(function (childSnap) {
    let snapKey = childSnap.key;
    let snapVal = childSnap.val();
    fetchKH(snapVal.id_khachhang).then(function (dataKH) {
      let loaixe = '';
      let typeStatus = '';
      if (snapVal.loaixe == 1) {
        loaixe = '<img src="./assets/images/vespa.svg" width="40" height="40" alt="">';
      } else {
        loaixe = '<img src="./assets/images/bike.svg" width="40" height="40" alt="">';
      }
      // 0 id_khachahng , 1 location , 2 id_taixe , 3 id_khachhang, 4 created_time , 5 sonhahem,  6 address,7 sdt, 8 loai xe, 9 address , 10 note,
      if (typeof snapVal.created_time === 'undefined') {
        snapVal.created_time = "0";
      }
      if (_.isEmpty(snapVal.location)) {
        snapVal.location = 0;
      }
      if (snapVal.status == 1) {
        typeStatus = '<strong class="warning"> Đã định vị xong</strong>';
      } else if (snapVal.status == 2) {
        typeStatus = '<strong class="danger">Chưa được định vị</strong>';
      } else if (snapVal.status == 3) {
        typeStatus = "Đang định vị";
      } else if (snapVal.status == 4) {
        typeStatus = '<strong class="info">Đang di chuyển </strong>'
      } else if (snapVal.status == 5) {
        typeStatus = '<strong class="success">Hoàn tất đường đi</strong>';
      } else if (snapVal.status == 6) {
        typeStatus = '<strong class="success">Đã có xe nhận</strong>';
      } else if (snapVal.status == 7) {

      } else if (snapVal.status == 8) {
        typeStatus = "Hệ thống đang chọn xe";
      }
      table.row.add([snapKey, snapVal.location, '006', snapVal.id_khachhang, snapVal.created_time, snapVal.sonhahem, snapVal.address,
        dataKH.sdt, loaixe, snapVal.sonhahem + " " + snapVal.address, typeStatus, snapVal.note, snapVal.status
      ]).draw(false);
    });
  });
});
$('#bookcar tbody').on('click', 'tr', function () {
  let data = table.row(this).data();
  var user = firebase.auth().currentUser;
  $('.page-carchoose').toggle();
  $('.page-listbook').toggle();
  // lay id tai xe tu dangkyxe
  $("#map_canvas").html();
  // if (user != null) {

  //   //updateDiem(data, dinhviDiem);

  // } else {
  //   console.log('Chưa đăng nhập');
  // }
});

function dinhviDiem(data) {

  $('.page-listbook').toggle();
  $('.page-carchoose').toggle("fast", "linear", function () {
    //console.log(data);
    codeAddress(data);
  });
}

function updateDiem(data, callback) {
  khachhang.data = data;
  khachhang.location = {};
  khachhang.mark = true;
  var postData = {
    updated_time: Date.now(),
    created_time: data[4],
    location: data[1],
    id_taixe: data[2],
    id_khachhang: data[3],
    address: data[6],
    note: data[10],
    status: 3, // dang xu ly
    sonhahem: data[5],
    uid: data[11]
  }
  // var updates = {};
  // updates['/diem/' + data[0]  ] = postData;
  rootRef.child('/diem/' + data[0]).set(postData).then(callback(postData), function (err) {
    // err here
  });
}

function fetchKH(id_khachhang) {
  return rootRef.child('khachhang').child(id_khachhang).once('value').then(function (snap) {
    return snap.val();
  });
}

function getLocation(id_location) {
  return rootRef.child('location').child(id_location).once('value').then(function (snap) {
    return snap.val();
  });
}

function getTaixe(id_taixe, addressKH) {
  var taixeRef;
  if (typeof id_taixe !== 'undefined' && id_taixe.length != 0) {
    taixeRef = rootRef.child('tai').child(id_taixe);
  } else taixeRef = rootRef.child('tai').orderByChild('status').equalTo(1);
  // endat deprecated use limitToFirst ;
  taixeRef.on('value', function (snap) {
    var listTaixe = {};
    listTaixe.dataLocation = [];
    listTaixe.data = [];
    var i = 0;
    snap.forEach(function (childSnap) {

      let snapKey = childSnap.key;
      let snapVal = childSnap.val();
      if (snapVal.status == 1 && typeof snapVal.location !== 'undefined') {
        listTaixe.dataLocation.push(snapVal.location);
        snapVal.pathKey = snapKey;
        listTaixe.data.push(snapVal);
        i++;
      }
      if (i == _.filter(snap.val(), function (o) {
          if (!_.isEmpty(o) && typeof o.location !== 'undefined') return o;
        }).length) { // fuck promise
        getDistance(listTaixe, [addressKH]);
      }
      // getLocation(snapVal.id_location).then(function(dataLocation){
      //   listTaixe.dataLocation.push( dataLocation );
      //   listTaixe.data.push(snapVal);
      // }).then(function(){

      // }) ;

    });
    //getDistance(listTaixe,[addressKH] );
  });
}
/* code map */
function codeAddress(data) {
  var geoData;
  var geocoder = new google.maps.Geocoder();
  if (typeof data.location === 'object') {
    geocoder.geocode({
      'location': data.location
    }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        // neu thanh cong
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        hienbando(lat, lng, data);
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });

  } else {
    geoData = data.sonhahem + " " + data.address;
    geocoder.geocode({
      'address': geoData
    }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        // neu thanh cong
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        hienbando(lat, lng, data);
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
  }

  //bd1 = new google.maps.Map(document.getElementById("map_canvas"), opt);
}

function hienbando(lat, lng, data) {
  khachhang.location = {
    lat: lat,
    lng: lng
  };
  var geoData;
  var opt = {
    center: new google.maps.LatLng(lat, lng),
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP //ROADMAP/SATELLITE/HYBRID/TERRAIN
  };
  bd1 = new google.maps.Map(document.getElementById("map_canvas"), opt);
  // tao poly
  // poly = new google.maps.Polyline({
  //   strokeColor: '#000000',
  //   strokeOpacity: 1.0,
  //   strokeWeight: 3
  // });
  // poly.setMap(bd1);
  //tạo maker, infowindow


  m1 = new google.maps.Marker({
    position: new google.maps.LatLng(lat, lng),
    draggable: true,
    animation: google.maps.Animation.DROP,
    map: bd1,
    title: 'trung tâm'
  });
  m1.addListener('dragend', function (e) {

    var te = m1.getPosition();
    khachhang.location = {
      lat: te.lat(),
      lng: te.lng()
    };
    khachhang.mark = true;
    console.log('keo tha vi tri');
    console.log(khachhang.location);
  });
  if (typeof khachhang.data !== 'undefined') {
    let constant = '<div id="content">' +
      '<div id="siteNotice">' + '</div>' +
      '<h1 id="firstHeading" class="firstHeading"></h1>' +
      '<div id="bodyContent">' +
      '<p><b>Khách hàng</b></p>' +
      '<p><b>Sdt</b>: ' + khachhang.data[7] + '</p>' +
      '<p><b>Địa chỉ</b>: ' + khachhang.data[9] + '</p>' +
      '<p><b>Ghi chú</b>: ' + khachhang.data[10] + '</p>' +
      '</div>' + '</div>';
    let infowindow = new google.maps.InfoWindow({
      content: constant
    });
    m1.addListener('click', function (e) {
      console.log('click map');
      infowindow.open(bd1, m1);
    });

  }
  //UpLatLng(lat,lng,"-1");// thêm toạ độ trung tâm
  var dem = 0;
  // google.maps.event.addListener(bd1, "click", function (e) {
  //   // xử lý kéo thả   
  //   var lat = e.latLng.lat();
  //   var lng = e.latLng.lng();
  //   //addMarker(lat,lng,bd1);
  // });
  if (typeof data.location === 'object') {
    getTaixe("", data.location);
  } else {
    geoData = data.sonhahem + " " + data.address;
    getTaixe("", geoData);
  }

}

function capNhatLatLng(lat, lng, ten) {

}

function getLatLng(id_location) {
  return locationRef.child(id_location).once('value').then(function (snap) {
    return snap.val();
  }).catch(function (err) {
    console.log('check lai getlatlng');
    return "";
  });
}

function getDistance(originArray, desinationArray) {
  console.log(originArray);
  console.log(desinationArray);
  // var origin1 = {lat: 10.764036451389597, lng: 106.66014635961915};
  // var origin2 = { lat:10.755435640815035 ,lng: 106.68615305822755 };
  // var destinationA = 'Stockholm, Sweden';
  // var destinationB = {lat: 10.762603, lng: 106.682634};
  var service = new google.maps.DistanceMatrixService();
  var tmp = [];
  //originArray = [ origin1,origin2, { lat: 10.771456561337404, lng: 106.71868288916016 } , { lat: 10.74970163083448, lng: 106.71713793676759 }, { lat: 10.741606372460339, lng: 106.69799769323731 }, { lat: 10.740004160180058, lng: 106.71662295263673 }   ];
  //desinationArray = [destinationB];
  service.getDistanceMatrix({
    origins: originArray.dataLocation,
    destinations: desinationArray,
    travelMode: 'DRIVING',
    // transitOptions: TransitOptions,
    // drivingOptions: DrivingOptions,
    unitSystem: google.maps.UnitSystem.METRIC,
    avoidHighways: false,
    avoidTolls: false,
  }, function (response, status) {
    if (status !== 'OK') {
      alert('Error was: ' + status);
    } else {
      var originList = response.originAddresses;
      var destinationList = response.destinationAddresses;

      deleteMarkers(markersArray);
      markersArray = [];
      for (var i = 0; i < originList.length; i++) {
        let results = response.rows[i].elements;
        for (var j = 0; j < results.length; j++) {
          // outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
          //           ': ' + results[j].distance.text + ' in ' +
          //           results[j].duration.text + '<br>';
          tmp.push({
            location: originArray.dataLocation[i],
            distance: results[j].distance.value,
            data: originArray.data[i]
          });
        }
        if (i == (originList.length - 1)) {
          tmp = _.take(_.orderBy(tmp, ['distance'], ['desc']), 10);
        }
      }
      for (let i = 0; i < tmp.length; i++) {
        addMarker(tmp[i].location.lat, tmp[i].location.lng);
      }
      for (let i = 0; i < markersArray.length; i++) {
        markersArray[i].addListener('click', function () {
          // console.log(tmp[i].data);
          if (tmp[i].data.status != 1) {
            alert('Hãy định vị trước khi chọn tài xế');
            return;
          } else {
            // console.log('đã dinh vi');
            // return;
            let dangkyxeRef = rootRef.child('dangkyxe');
            var newpostKey = dangkyxeRef.push().key;
            let insertDangkyxeRef = dangkyxeRef.child(newpostKey);
            let locationTmp;
            if (_.isEmpty(khachhang.location)) {
              geocoder.geocode({
                'address': khachhang.data[9]
              }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  locationTmp = results[0].geometry.location;
                } else {
                  alert("Geocode was not successful for the following reason: " + status);
                }
              });
            } else {
              locationTmp = khachhang.location;
            }
            let postData = {
              created_time: Date.now(),
              des: locationTmp,
              id_diem: khachhang.data[0],
              id_taixe: tmp[i].data.uid,
              start: tmp[i].location,
              status: 3
            };

            insertDangkyxeRef.set(postData).then(function () {
              console.log(' thêm dang ky xe thành công');
              let updates = {};
              updates['/status'] = 4;
              updates['/waitAccept'] = {
                ChuaXN: 1,
                id_dangkyxe: newpostKey,
                address: khachhang.data[9],
                sdt: khachhang.data[7],
                new: 1,
                distance: tmp[i].distance,
                id_diem: khachhang.data[0]
              };
              rootRef.child('tai').child(tmp[i].data.pathKey).update(updates).then(function () {
                console.log(' đang chờ xác nhận');
                $('.page-carchoose').toggle();
                $('.page-listbook').toggle();
              }).catch(function (err) {
                console.log('check update taixe status , waite accept');
              });
            }).catch(function (err) {
              console.log(err);
            });
          }
        });
      }
    }
  });

}

function deleteMarkers(markersArray) {
  for (var i = 0; i < markersArray.length; i++) {
    markersArray[i].setMap(null);
  }
  markersArray = [];
}

function addMarker(lat, lng) {
  // Add the marker at the clicked location, and add the next-available label
  // from the array of alphabetical characters.
  var objlatlng = new google.maps.LatLng(lat, lng);
  markersArray.push(new google.maps.Marker({
    position: objlatlng,
    icon: {
      url: 'assets/images/bike.svg',
      scaledSize: new google.maps.Size(50, 25), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0) // anchor 
    },
    map: bd1
  }));
}
/** end codemap */
$('document').ready(function (e) {
  $('.page-carchoose').toggle();
  $('#page-list').click(function (e) {
    $('.page-carchoose').toggle();
    $('.page-listbook').toggle();
  });

});